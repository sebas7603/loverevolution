<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Registro - Love Revolution</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('vendor/materialize/css/materialize.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/material.io/material-components-web.min.css') }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container py-4 px-2">
            <div class="row mb-5">
                <div class="col s12 center">
                    <img src="{{ asset('images/logo.jpg') }}" alt="Logo Love Revolution" class="main-logo">
                    <h1>Registro de Asistentes</h1>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col s12">
                    <h3 class="mb-2"><span class="fancy-number">1</span> Selección de Reunión</h3>
                    <input type="text" class="hide" id="meet_data">
                </div>
                <div class="col s12">
                    <div class="row mb-0">
                        @for ($i = 0; $i < count($meets); $i++)
                            <div class="col s12 m6 xl4">
                                <div class="card highlighted meet--option noselect mt-0 mb-1" data-value="{{ date('Y-m-d H:i:s', strtotime('next sunday' . $meets[$i])) }}">
                                    <div class="card-content">
                                        <h3>{{ $meets[$i] }}</h3>
                                        <span class="card-title">{{ $nextSunday }}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col s12">
                    <h3 class="mb-2"><span class="fancy-number">2</span> Inscripción de Asistentes</h3>
                </div>
                <div class="col s12">
                    <div class="row mb-0">
                        <div class="col s12 m8">
                            <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense" id="mdc-cedula">
                                <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_cedula" id="cedula" value="">
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span class="mdc-floating-label" id="lbl_cedula">Número de Cédula</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </label>
                        </div>
                        <div class="col s12 m4 mt-1-s">
                            <button class="mdc-button mdc-button--outlined mdc-button--x-large full-width" id="agregar" type="button">
                                <span class="mdc-button__label mr-1">Agregar</span>
                                <i class="material-icons send">add</i>
                                <i class="material-icons sending" style="display: none">loop</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <h3 class="mb-2"><span class="fancy-number">3</span> Listado de Personas</h3>
                </div>
                <div class="col s12">
                    <div class="row mb-0" id="row-lista-vacia" style="display: none">
                        <div class="col s12">
                            <div class="card noselect mt-0 mb-1" id="card-lista-vacia">
                                <div class="card-content">
                                    <h3 class="no-margin">No Hay Personas Inscritas</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-0" id="row-lista-data">
                        <div class="col s12">
                            <div class="card highlighted noselect mt-0 mb-1 assistant-card">
                                <div class="card-content">
                                    <div class="row mb-0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="person_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
                <div class="row mb-0 div-nuevo-cliente">
                    <div class="col s12">
                        <h3 class="title">DATOS DEL ASISTENTE</h3>
                        <p class="my-0 white-text">No te preocupes, sólo se pedirán una vez.</p>
                    </div>
                </div>
                <div class="row mb-0 div-editar-cliente" style="display: none;">
                    <div class="col s12">
                        <h4 class="title">Editar cliente</h4>
                        <p class="my-0">Cambie los datos que considere necesarios. El cliente se asignará al pedido automáticamente.</p>
                    </div>
                </div>
                <div class="row mb-0 mt-3">
                    <div class="col s8 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-num_cedula">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_num_cedula" name="num_cedula" id="num_cedula" readonly>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_num_cedula">Cédula</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s4 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-edad">
                            <input type="number" class="mdc-text-field__input" aria-labelledby="lbl_edad" name="edad" id="edad" min="7" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_edad">Edad</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s12 m6 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-nombres">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_nombres" name="nombres" id="nombres" minlength="3" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_nombres">Nombres</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s12 m6 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-apellidos">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_apellidos" name="apellidos" id="apellidos" minlength="3" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_apellidos">Apellidos</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s12 m6 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-telefono">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_telefono" name="telefono" id="telefono" minlength="7" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_telefono">Teléfono</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s12 m6 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense"  id="mdc-direccion">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_direccion" name="direccion" id="direccion" minlength="5" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_direccion">Dirección</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                    <div class="col s12 mt-2">
                        <label class="mdc-text-field mdc-text-field--outlined mdc-text-field--dense" id="mdc-barrio">
                            <input type="text" class="mdc-text-field__input" aria-labelledby="lbl_barrio" name="barrio" id="barrio" minlength="3" required>
                            <span class="mdc-notched-outline">
                                <span class="mdc-notched-outline__leading"></span>
                                <span class="mdc-notched-outline__notch">
                                    <span class="mdc-floating-label" id="lbl_barrio">Barrio</span>
                                </span>
                                <span class="mdc-notched-outline__trailing"></span>
                            </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row mb-0">
                    <div class="col s12 right-align div-nuevo-persona">
                        <button class="mdc-button mdc-button--outlined mdc-button--large modal-close" id="salir_crear_persona" type="button">
                            <span class="mdc-button__label">Salir</span>
                        </button>
                        <button class="mdc-button mdc-button--raised mdc-button--large guardar_persona" id="crear_persona" type="button">
                            <span class="mdc-button__label mr-1">Guardar Datos</span>
                            <i class="material-icons">save</i>
                        </button>
                    </div>
                    <div class="col s12 right-align div-editar-persona" style="display: none;">
                        <button class="mdc-button mdc-button--outlined mdc-button--large modal-close" id="salir_actualizar_persona" type="button">
                            <span class="mdc-button__label">Salir</span>
                        </button>
                        <button class="mdc-button mdc-button--raised mdc-button--large guardar_persona" id="actualizar_persona" type="button">
                            <span class="mdc-button__label mr-1">Actualizar</span>
                            <i class="material-icons">save</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="symptoms_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
                <div class="row mb-0">
                    <div class="col s12">
                        <h3 class="title">REPORTE DE SÍNTOMAS</h3>
                        <p class="my-0 white-text">Hola <span class="capitalize" id="symptoms-name"></span>, indícanos si, durante esta semana, has presentado alguno de los siguientes síntomas.</p>
                    </div>
                </div>
                <div class="row mb-0 mt-3">
                    <div class="col s6 m4 mb-2">
                        <div class="mdc-select mdc-select--outlined mdc-select--dense mdc-select--required" id="mdc-fiebre">
                            <div class="mdc-select__anchor" aria-labelledby="mdc-fiebre-label" aria-required="true">
                                <span id="fiebre__selected-text" class="mdc-select__selected-text"></span>
                                <span class="mdc-select__dropdown-icon">
                                    <svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5">
                                        <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>
                                        <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>
                                    </svg>
                                </span>
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span id="mdc-fiebre-label" class="mdc-floating-label">Fiebre</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </div>
                            <!-- Other elements from the select remain. -->
                            <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
                                <ul class="mdc-list">
                                    <li class="mdc-list-item" data-value="0" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">No</span>
                                    </li>
                                    <li class="mdc-list-item" data-value="1" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">Si</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m4 mb-2">
                        <div class="mdc-select mdc-select--outlined mdc-select--dense mdc-select--required" id="mdc-tos">
                            <div class="mdc-select__anchor" aria-labelledby="mdc-tos-label" aria-required="true">
                                <span id="tos__selected-text" class="mdc-select__selected-text"></span>
                                <span class="mdc-select__dropdown-icon">
                                    <svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5">
                                        <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>
                                        <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>
                                    </svg>
                                </span>
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span id="mdc-tos-label" class="mdc-floating-label">Tos</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </div>
                            <!-- Other elements from the select remain. -->
                            <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
                                <ul class="mdc-list">
                                    <li class="mdc-list-item" data-value="0" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">No</span>
                                    </li>
                                    <li class="mdc-list-item" data-value="1" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">Si</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m4 mb-2">
                        <div class="mdc-select mdc-select--outlined mdc-select--dense mdc-select--required" id="mdc-mocos">
                            <div class="mdc-select__anchor" aria-labelledby="mdc-mocos-label" aria-required="true">
                                <span id="mocos__selected-text" class="mdc-select__selected-text"></span>
                                <span class="mdc-select__dropdown-icon">
                                    <svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5">
                                        <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>
                                        <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>
                                    </svg>
                                </span>
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span id="mdc-mocos-label" class="mdc-floating-label">Secreción Nasal</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </div>
                            <!-- Other elements from the select remain. -->
                            <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
                                <ul class="mdc-list">
                                    <li class="mdc-list-item" data-value="0" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">No</span>
                                    </li>
                                    <li class="mdc-list-item" data-value="1" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">Si</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 mb-2">
                        <div class="mdc-select mdc-select--outlined mdc-select--dense mdc-select--required" id="mdc-cefalea">
                            <div class="mdc-select__anchor" aria-labelledby="mdc-cefalea-label" aria-required="true">
                                <span id="cefalea__selected-text" class="mdc-select__selected-text"></span>
                                <span class="mdc-select__dropdown-icon">
                                    <svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5">
                                        <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>
                                        <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>
                                    </svg>
                                </span>
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span id="mdc-cefalea-label" class="mdc-floating-label">Cefalea</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </div>
                            <!-- Other elements from the select remain. -->
                            <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
                                <ul class="mdc-list">
                                    <li class="mdc-list-item" data-value="0" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">No</span>
                                    </li>
                                    <li class="mdc-list-item" data-value="1" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">Si</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 mb-2">
                        <div class="mdc-select mdc-select--outlined mdc-select--dense mdc-select--required" id="mdc-respirar">
                            <div class="mdc-select__anchor" aria-labelledby="mdc-respirar-label" aria-required="true">
                                <span id="respirar__selected-text" class="mdc-select__selected-text"></span>
                                <span class="mdc-select__dropdown-icon">
                                    <svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5">
                                        <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>
                                        <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>
                                    </svg>
                                </span>
                                <span class="mdc-notched-outline">
                                    <span class="mdc-notched-outline__leading"></span>
                                    <span class="mdc-notched-outline__notch">
                                        <span id="mdc-respirar-label" class="mdc-floating-label">Dificultad para Respirar</span>
                                    </span>
                                    <span class="mdc-notched-outline__trailing"></span>
                                </span>
                            </div>
                            <!-- Other elements from the select remain. -->
                            <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
                                <ul class="mdc-list">
                                    <li class="mdc-list-item" data-value="0" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">No</span>
                                    </li>
                                    <li class="mdc-list-item" data-value="1" role="option">
                                        <span class="mdc-list-item__ripple"></span>
                                        <span class="mdc-list-item__text">Si</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row mb-0">
                    <div class="col s12 right-align div-nuevo-persona">
                        <button class="mdc-button mdc-button--outlined mdc-button--large modal-close" id="salir_sintomas" type="button">
                            <span class="mdc-button__label">Salir</span>
                        </button>
                        <button class="mdc-button mdc-button--raised mdc-button--large guardar_persona" id="sintomas" type="button">
                            <span class="mdc-button__label mr-1">Confirmar</span>
                            <i class="material-icons">save</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('vendor/jquery/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('vendor/materialize/js/materialize.min.js') }}"></script>
        <script src="{{ asset('vendor/material.io/material-components-web.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                const cedula = new mdc.textField.MDCTextField(document.querySelector('#mdc-cedula'));
                const num_cedula = new mdc.textField.MDCTextField(document.querySelector('#mdc-num_cedula'));
                const edad = new mdc.textField.MDCTextField(document.querySelector('#mdc-edad'));
                const nombres = new mdc.textField.MDCTextField(document.querySelector('#mdc-nombres'));
                const apellidos = new mdc.textField.MDCTextField(document.querySelector('#mdc-apellidos'));
                const telefono = new mdc.textField.MDCTextField(document.querySelector('#mdc-telefono'));
                const direccion = new mdc.textField.MDCTextField(document.querySelector('#mdc-direccion'));
                const barrio = new mdc.textField.MDCTextField(document.querySelector('#mdc-barrio'));

                const fiebre = new mdc.select.MDCSelect(document.querySelector('#mdc-fiebre'));
                const tos = new mdc.select.MDCSelect(document.querySelector('#mdc-tos'));
                const mocos = new mdc.select.MDCSelect(document.querySelector('#mdc-mocos'));
                const cefalea = new mdc.select.MDCSelect(document.querySelector('#mdc-cefalea'));
                const respirar = new mdc.select.MDCSelect(document.querySelector('#mdc-respirar'));

                var urlGetData = '{!! route('getData', ['cedula' => 'null']) !!}';
                var urlValidate = '{!! route('validateAssistant', ['id' => 'idPerson', 'meet' => 'meetData']) !!}';
                var urlPersonCreate = '{!! route('person.create') !!}';
                var urlAssistantCreate = '{!! route('assistant.create') !!}';

                var person = null;
                var assistants = [];
                if (localStorage.assistants) {
                    assistants = JSON.parse(localStorage.assistants);
                }

                listAssistants();

                $('#person_modal').modal({
                    dismissible: false,
                    onCloseEnd: function(modal, trigger) {
                        num_cedula.value = '';
                        num_cedula.valid = true;
                        edad.value = '';
                        edad.valid = true;
                        nombres.value = '';
                        nombres.valid = true;
                        apellidos.value = '';
                        apellidos.valid = true;
                        telefono.value = '';
                        telefono.valid = true;
                        direccion.value = '';
                        direccion.valid = true;
                        barrio.value = '';
                        barrio.valid = true;
                        $('#nuevo_cliente_modal .mdc-text-field').removeClass('mdc-text-field--label-floating');
                        $('#nuevo_cliente_modal .mdc-text-field .mdc-notched-outline').removeClass('mdc-notched-outline--notched');
                        $('#nuevo_cliente_modal .mdc-text-field .mdc-floating-label').removeClass('mdc-floating-label--float-above');
                    }
                });

                $('#symptoms_modal').modal({
                    dismissible: false,
                    onOpenStart: function(modal, trigger) {
                        fiebre.value = '';
                        fiebre.valid = true;
                        tos.value = '';
                        tos.valid = true;
                        mocos.value = '';
                        mocos.valid = true;
                        cefalea.value = '';
                        cefalea.valid = true;
                        respirar.value = '';
                        respirar.valid = true;
                    },
                    onCloseEnd: function(modal, trigger) {
                        cedula.value = '';
                        cedula.valid = true;
                        $('#mdc-cedula').removeClass('mdc-text-field--label-floating');
                        $('#mdc-cedula .mdc-notched-outline').removeClass('mdc-notched-outline--notched');
                        $('#mdc-cedula .mdc-floating-label').removeClass('mdc-floating-label--float-above');
                    }
                });

                $('.meet--option').click(function() {
                    $('.meet--option').removeClass('option--selected');
                    $(this).addClass('option--selected');
                    $('#meet_data').val($(this).data('value')).attr('value', $(this).data('value'));
                });

                $('#agregar').click(function() {
                    console.log('ctrl');
                    console.log(cedula.value.length);
                    if (cedula.value && cedula.value.length >= 7) {
                        let url = urlGetData.replace('null', cedula.value);
                        $.ajax({
                            method: 'GET',
                            url: url,
                            context: document.body,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        }).done(function(response){
                            console.log(response);
                            if (response.person) {
                                person = response.person;
                                $('#symptoms-name').text(person.names.toLowerCase());
                                $('#person_modal').modal('close');
                                validarAsistencia(person.id);
                            } else {
                                num_cedula.value = cedula.value;
                                $('#person_modal').modal('open');
                            }
                        }).fail(function(error){
                            console.log(error);
                        });
                    } else {
                        M.toast({html: 'Ingresa un número cédula válido.'});
                    }
                });

                $('#crear_persona').click(function() {
                    let valEdad = (edad.value && edad.valid);
                    let valNombres = (nombres.value && nombres.valid);
                    let valApellidos = (apellidos.value && apellidos.valid);
                    let valTelefono = (telefono.value && telefono.valid);
                    let valDireccion = (direccion.value && direccion.valid);
                    let valBarrio = (barrio.value && barrio.valid);
                    if (valEdad && valNombres && valApellidos && valTelefono && valDireccion && valBarrio) {
                        $.ajax({
                            method: 'POST',
                            url: urlPersonCreate,
                            context: document.body,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                document: num_cedula.value,
                                names: nombres.value ,
                                lastnames: apellidos.value ,
                                age: edad.value ,
                                phone_number: telefono.value ,
                                address: direccion.value ,
                                neighborhood: barrio.value 
                            }
                        }).done(function(response){
                            console.log(response);
                            if (response.person) {
                                person = response.person;
                                $('#symptoms-name').text(person.names.toLowerCase());
                                $('#person_modal').modal('close');
                                validarAsistencia(person.id);
                            } else {
                                M.toast({html: 'Ocurrió un error inesperado. Intenta más tarde.'});
                            }
                        }).fail(function(error){
                            M.toast({html: 'Ocurrió un error inesperado. Intenta más tarde.'});
                            console.log(error);
                        });
                    } else {
                        M.toast({html: 'Alguno de los datos no es válido.'});
                        console.log('no paso');
                    }
                });

                function validarAsistencia(idPerson) {
                    if ($('#meet_data').val()) {
                        let url = urlValidate.replace('idPerson', idPerson).replace('meetData', $('#meet_data').val()).replace(' ', '%20');
                        $.ajax({
                            method: 'GET',
                            url: url,
                            context: document.body,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        }).done(function(response){
                            if (response.assistant) {
                                let result = null;
                                if (assistants) {
                                    for (let index = 0; index < assistants.length; index++) {
                                        let ass = assistants[index];
                                        if (ass.id == response.assistant.id) {
                                            result = ass;
                                            break;
                                        }
                                    }
                                }
                                if (!result) {
                                    assistants.push(response.assistant);
                                    localStorage.assistants = JSON.stringify(assistants);
                                    listAssistants();
                                }
                                M.toast({html: response.assistant.person.names + ' ya está registrado en esa reunión.'});
                            } else {
                                $('#symptoms_modal').modal('open');
                            }
                        }).fail(function(error){
                            console.log(error);
                        });
                    } else {
                        M.toast({html: 'Selecciona una reunión.'});
                    }
                }

                $('#sintomas').click(function() {
                    let valFiebre = (fiebre.value && fiebre.valid);
                    let valTos = (tos.value && tos.valid);
                    let valMocos = (mocos.value && mocos.valid);
                    let valCefalea = (cefalea.value && cefalea.valid);
                    let valRespirar = (respirar.value && respirar.valid);

                    if (valFiebre && valTos && valMocos && valCefalea && valRespirar) {
                        $.ajax({
                            method: 'POST',
                            url: urlAssistantCreate,
                            context: document.body,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                person_id: person.id,
                                meet: $('#meet_data').val(),
                                fever: fiebre.value ,
                                cough: tos.value ,
                                mucus: mocos.value ,
                                head_pain: cefalea.value ,
                                difficult_breath: respirar.value 
                            }
                        }).done(function(response){
                            if (response.assistant) {
                                let result = null;
                                if (assistants) {
                                    for (let index = 0; index < assistants.length; index++) {
                                        let ass = assistants[index];
                                        if (ass.id == response.assistant.id) {
                                            result = ass;
                                            break;
                                        }
                                    }
                                }
                                if (!result) {
                                    assistants.push(response.assistant);
                                    localStorage.assistants = JSON.stringify(assistants);
                                    listAssistants();
                                }
                                M.toast({html: response.assistant.person.names + ' se agregó correctamente.'});
                            } else if (response.quantity) {
                                M.toast({html: 'Ya no hay cupos para esta reunión.'});
                            } else {
                                M.toast({html: 'Ocurrió un error inesperado. Intenta más tarde.'});
                            }
                            $('.meet--option').removeClass('option--selected');
                            $('#meet_data').val('').attr('value', '');
                            $('#symptoms_modal').modal('close');
                        }).fail(function(error){
                            M.toast({html: 'Ocurrió un error inesperado. Intenta más tarde.'});
                            console.log(error);
                        });
                    } else {
                        M.toast({html: 'Por favor, llena todos los síntomas.'});
                    }
                });

                function listAssistants() {
                    $('#row-lista-data').empty();
                    if (assistants != null && assistants.length) {
                        for (let index = 0; index < assistants.length; index++) {
                            const element = assistants[index];
                            let fecha = new Date(element.meet);
                            let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour12: true };
                            console.log();
                            let nombre = '<div class="col s12 m6 mb-1"><label>Nombre Completo</label><span class="ass_nombre card-subtitle subtitle-2 highlighted nowrap">'+ element.person.names +' '+ element.person.lastnames +'</span></div>';
                            let cedula = '<div class="col s6 m3 mb-1"><label>Cédula</label><span class="ass_cedula card-subtitle subtitle-2 highlighted nowrap">'+ element.person.document +'</span></div>';
                            let telefono = '<div class="col s6 m3 mb-1"><label>Teléfono</label><span class="ass_telefono card-subtitle subtitle-2 highlighted nowrap">'+ element.person.phone_number +'</span></div>';
                            let direccion = '<div class="col s12 m4 mb-1"><label>Dirección</label><span class="ass_direccion card-subtitle subtitle-2 highlighted nowrap">'+ element.person.address +'</span></div>';
                            let barrio = '<div class="col s12 m4 mb-1"><label>Barrio</label><span class="ass_barrio card-subtitle subtitle-2 highlighted nowrap">'+ element.person.neighborhood +'</span></div>';
                            let reunion = '<div class="col s12 m4 mb-1"><label>Reunión</label><span class="ass_reunion card-subtitle subtitle-2 highlighted important nowrap">'+ fecha.toLocaleString('es-CO', {month: 'short', day: 'numeric', hour: '2-digit', minute:'2-digit', hour12: true}) +'</span></div>';
                            let fiebre = '<div class="col s4 m2 m-col-5 mb-1"><label>Fiebre</label><span class="ass_fiebre card-subtitle subtitle-2 highlighted nowrap">'+ (element.fever == '0' ? 'NO' : 'SI') +'</span></div>';
                            let tos = '<div class="col s4 m2 m-col-5 mb-1"><label>Tos</label><span class="ass_tos card-subtitle subtitle-2 highlighted nowrap">'+ (element.cough == '0' ? 'NO' : 'SI') +'</span></div>';
                            let mocos = '<div class="col s4 m2 m-col-5 mb-1"><label>Sec. Nasal</label><span class="ass_mocos card-subtitle subtitle-2 highlighted nowrap">'+ (element.mucus == '0' ? 'NO' : 'SI') +'</span></div>';
                            let cefalea = '<div class="col s6 m2 m-col-5 mb-1"><label>Cefálea</label><span class="ass_cefalea card-subtitle subtitle-2 highlighted nowrap">'+ (element.head_pain == '0' ? 'NO' : 'SI') +'</span></div>';
                            let respira = '<div class="col s6 m2 m-col-5 mb-1"><label>Dif. Respiratoria</label><span class="ass_respiratoria card-subtitle subtitle-2 highlighted nowrap">'+ (element.difficult_breath == '0' ? 'NO' : 'SI') +'</span></div>';
                            let data = nombre + cedula + telefono + direccion + barrio + reunion + fiebre + tos + mocos + cefalea + respira;
                            let fila = '<div class="col s12"><div class="card highlighted noselect mt-0 mb-1 assistant-card"><div class="card-content"><div class="row mb-0">'+ data +'</div></div></div></div>';
                            $('#row-lista-data').append(fila);
                        }
                        $('#row-lista-vacia').hide();
                        $('#row-lista-data').show();
                    } else {
                        $('#row-lista-vacia').show();
                        $('#row-lista-data').hide();
                    }
                }
            });
        </script>
    </body>
</html>
