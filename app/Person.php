<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'id', 'document', 'names', 'lastnames', 'age', 'phone_number', 'address', 'neighborhood'
    ];

    public function assistances() {
        return $this->hasMany('App\Assitant', 'person_id', 'id');
    }
}
