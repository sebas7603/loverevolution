<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assistant extends Model
{
    protected $fillable = [
        'person_id', 'meet', 'fever', 'cough', 'mucus', 'head_pain', 'difficult_breath'
    ];

    public function person() {
        return $this->belongsTo('App\Person');
    }
}
