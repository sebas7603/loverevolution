<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Assistant;

class PublicController extends Controller
{
    public function index() {
        $nextSunday = date('d/m/Y', strtotime('next sunday'));
        $meets = [date("h:i a", strtotime('9:00:00')), date("h:i a", strtotime('11:00:00'))];
        //dd($meets, 'si');
        return view('welcome')->with(compact('nextSunday', 'meets'));
    }

    public function getData($cedula) {
        $person = Person::where('document', $cedula)->first();
        return response()->json(compact('person'), 200);
    }

    public function personCreate(Request $request) {
        $person = new Person();
        $person->fill($request->all());
        $person->save();
        return response()->json(compact('person'), 200);
    }

    public function validateAssistant($id, $meet) {
        $person = null;
        $assistant = Assistant::where('person_id', $id)->where('meet', $meet)->first();
        if ($assistant) {
            $person = $assistant->person;
        }
        return response()->json(compact('assistant', 'person'), 200);
    }

    public function assistantCreate(Request $request) {
        $quantity = Assistant::where('meet', $request->meet)->count();
        if ($quantity < 50) {
            $assistant = new Assistant();
            $assistant->fill($request->all());
            $assistant->save();
            $person = $assistant->person;
            return response()->json(compact('assistant', 'person'), 200);
        } else {
            return response()->json(compact('quantity'), 200);
        }
    }
}
